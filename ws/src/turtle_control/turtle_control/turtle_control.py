"""Module for controlling the turtlesim."""

from threading import Event, Lock
import math
import colorsys
import rclpy
from rclpy.node import Node
from rclpy.action import ActionServer
from rclpy.executors import MultiThreadedExecutor
from rclpy.callback_groups import MutuallyExclusiveCallbackGroup

from geometry_msgs.msg import Twist
from turtlesim.msg import Pose
from turtlesim.srv import SetPen

from turtle_core.action import Target


class TurtleControl(Node):
    """Node for listening to target goals and controlling the turtlesim."""

    def __init__(self):
        """Initialize the node."""
        super().__init__("turtle_control")
        # multi-threading so we can have an action running concurrently with the other callbacks
        self.target_lock = Lock()
        self.target_reached_event = Event()
        self.target_callback_group = MutuallyExclusiveCallbackGroup()

        # current target
        self.target_x = 7.5
        self.target_y = 5.5

        # current distance to target
        self.dist_diff = 0.0

        # counter for the color callback
        self.i = 0

        # initialize subscriber for the robot's position
        # TODO: create a subscriber for the robot's position
        self.pose_subscriber = None

        # initialize publisher for the robot's velocity
        # TODO: create a publisher for the robot's velocity
        self.cmd_vel_publisher = None

        # initialize service client for the robot's pen
        self.color_client = self.create_client(SetPen, "/turtle1/set_pen")
        # wait for the service to be available
        while not self.color_client.wait_for_service(timeout_sec=1.0):
            self.get_logger().info("/turtle1/set_pen service not available, waiting again...")

        # run the color callback every 1 / 30 seconds
        self.timer = self.create_timer(1 / 30, self.color_callback)

        # initialize action server for the robot's target
        self.action_server = ActionServer(
            self, Target, "target", self.target_callback, callback_group=self.target_callback_group
        )

    def pose_callback(self, pose: Pose):
        """Update the robot's position and publish new velocity based on current target."""
        with self.target_lock:
            # calculate the distance and angle to the target
            target_angle = math.atan2(self.target_y - pose.y, self.target_x - pose.x)
            angle_diff = math.atan2(
                math.sin(pose.theta - target_angle), math.cos(pose.theta - target_angle)
            )
            self.dist_diff = math.hypot(self.target_x - pose.x, self.target_y - pose.y)

            cmd_msg = Twist()
            # if the angle difference is too big, don't move forward to avoid overshooting
            if self.dist_diff > 0.1 and abs(angle_diff) > 0.1:
                cmd_msg.linear.x = 0.0
                cmd_msg.angular.z = max(-1.0, min(1.0, -angle_diff))
            elif self.dist_diff > 0.1:
                cmd_msg.linear.x = min(1.0, self.dist_diff)
                cmd_msg.angular.z = max(-1.0, min(1.0, -angle_diff))
            else:
                self.target_reached_event.set()

        # TODO: publish the velocity command

    def color_callback(self):
        """Update the robot's pen color to have a rainbow effect."""
        pen_req = SetPen.Request()
        pen_req.off = 0
        pen_req.width = 3
        pen_req.r, pen_req.g, pen_req.b = tuple(
            round(i * 255) for i in colorsys.hsv_to_rgb(self.i / 60, 1.0, 1.0)
        )
        self.i = (self.i + 1) % 60

        # Use call_async to prevent deadlocks
        # (https://docs.ros.org/en/humble/How-To-Guides/Sync-Vs-Async.html)

        # TODO: call the service to set the pen color

    def target_callback(self, target_handle):
        """Update the robot's target."""
        self.get_logger().info("Received goal request")
        with self.target_lock:
            self.target_x = target_handle.request.x
            self.target_y = target_handle.request.y
            self.target_reached_event.clear()

        while not self.target_reached_event.wait(timeout=1.0):
            feedback_msg = Target.Feedback()
            feedback_msg.dist = self.dist_diff
            target_handle.publish_feedback(feedback_msg)

        target_handle.succeed()

        result = Target.Result()
        result.success = True
        self.get_logger().info("Sending result")
        return result


def main(args=None):
    """Run TurtleControl node."""
    rclpy.init(args=args)

    turtle_control = TurtleControl()

    executor = MultiThreadedExecutor()
    rclpy.spin(turtle_control, executor)


if __name__ == "__main__":
    main()
