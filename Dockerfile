FROM ubuntu:jammy

# setup environment
SHELL ["/bin/bash", "-c"]
ENV DEBIAN_FRONTEND=noninteractive
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8

RUN apt-get -q -y update && apt-get -q -y upgrade

# setup timezone
RUN echo 'Etc/UTC' > /etc/timezone && \
    ln -s /usr/share/zoneinfo/Etc/UTC /etc/localtime && \
    apt-get -q -y install tzdata

# setup locale
RUN apt-get -q -y install locales && \
    locale-gen en_US en_US.UTF-8 && \
    update-locale LC_ALL=en_US.UTF-8 LANG=en_US.UTF-8

# add universe repository
RUN apt-get -q -y install software-properties-common && \
    add-apt-repository universe

# install basic packages
RUN apt-get -q -y install \
    git \
    gnupg gnupg2 dirmngr \
    ca-certificates \
    coreutils \
    sudo wget curl unzip \
    lsb-release pkg-config \
    apt-utils \
    bash-completion \
    build-essential ccache ninja-build cmake \
    clang clang-format \
    gdb \
    python3 \
    python3-dev \
    python3-apt \
    python3-pip \
    iputils-ping iproute2 \
    neovim tmux

# setup keys
RUN apt-get -q -y update && apt-get -q -y install curl
RUN curl -sSL https://raw.githubusercontent.com/ros/rosdistro/master/ros.key -o /usr/share/keyrings/ros-archive-keyring.gpg

# setup sources.list
RUN echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/ros-archive-keyring.gpg] http://packages.ros.org/ros2/ubuntu $(. /etc/os-release && echo $UBUNTU_CODENAME) main" | tee /etc/apt/sources.list.d/ros2.list > /dev/null

# update apt repositories and install bootstrap tools
RUN apt-get -q -y update && apt-get -q -y install \
    python3-colcon-common-extensions \
    python3-colcon-mixin \
    python3-rosdep \
    python3-vcstool

# install python packages
RUN pip3 install -U \
    setuptools==58.2.0 \
    argcomplete \
    flake8 \
    flake8-builtins \
    flake8-comprehensions \
    flake8-deprecated \
    flake8-docstrings \
    flake8-quotes \
    pytest \
    pytest-repeat \
    pytest-rerunfailures \
    black

# install ros2
RUN apt-get -q -y install ros-humble-desktop ros-dev-tools

RUN echo -e "\nsource /opt/ros/humble/setup.bash" >> /root/.bashrc

RUN echo -e "\nsource /usr/share/colcon_argcomplete/hook/colcon-argcomplete.bash" >> /root/.bashrc

WORKDIR /onboarding_2022

CMD ["bash"]
